-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3307
-- Generation Time: 02-Out-2014 às 06:38
-- Versão do servidor: 5.6.15-log
-- PHP Version: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `delodi`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `survey`
--

CREATE TABLE IF NOT EXISTS `survey` (
  `survey_id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_name` varchar(50) NOT NULL,
  PRIMARY KEY (`survey_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf16 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `survey`
--

INSERT INTO `survey` (`survey_id`, `survey_name`) VALUES
(1, 'Delodi Survey');

-- --------------------------------------------------------

--
-- Estrutura da tabela `survey_questions`
--

CREATE TABLE IF NOT EXISTS `survey_questions` (
  `survey_questions_id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) NOT NULL,
  `survey_questions_text` varchar(400) NOT NULL,
  `survey_questions_type` int(11) NOT NULL,
  PRIMARY KEY (`survey_questions_id`),
  KEY `survey_id` (`survey_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf16 AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `survey_questions`
--

INSERT INTO `survey_questions` (`survey_questions_id`, `survey_id`, `survey_questions_text`, `survey_questions_type`) VALUES
(1, 1, 'Which type of occupation discribes you best?', 1),
(2, 1, 'How often do you visit our website', 1),
(3, 1, 'In which of our website''s topics are you mostly interested in? (multiple options)', 2),
(4, 1, 'Which other topics would you like to fnd on our website? Please sort them according to your preferences: - (multiple options)', 2),
(5, 1, 'Are there any other people living with You?', 3),
(6, 1, 'How old are you?', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `survey_questions_options`
--

CREATE TABLE IF NOT EXISTS `survey_questions_options` (
  `survey_questions_options_id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_questions_id` int(11) NOT NULL,
  `survey_questions_options_text` varchar(400) NOT NULL,
  PRIMARY KEY (`survey_questions_options_id`),
  KEY `survey_questions_id` (`survey_questions_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf16 AUTO_INCREMENT=28 ;

--
-- Extraindo dados da tabela `survey_questions_options`
--

INSERT INTO `survey_questions_options` (`survey_questions_options_id`, `survey_questions_id`, `survey_questions_options_text`) VALUES
(1, 1, 'pupil'),
(2, 1, 'student'),
(3, 1, 'employee'),
(4, 1, 'civil servent'),
(5, 1, 'self employed'),
(6, 1, 'other'),
(7, 2, '1-3 times'),
(8, 2, '3-5 times'),
(9, 2, 'more than 5 times'),
(10, 3, 'politics'),
(11, 3, 'culture'),
(12, 3, 'sports'),
(13, 3, 'people'),
(14, 3, 'business'),
(15, 4, 'music'),
(16, 4, 'internet'),
(17, 4, 'local'),
(18, 4, 'travel'),
(19, 4, 'cars'),
(21, 5, 'yes'),
(20, 5, 'no'),
(22, 6, 'below 18'),
(23, 6, '18-25'),
(24, 6, '25-32'),
(25, 6, '32-40'),
(26, 6, '41-50'),
(27, 6, 'over 50');

-- --------------------------------------------------------

--
-- Estrutura da tabela `survey_questions_options_plus`
--

CREATE TABLE IF NOT EXISTS `survey_questions_options_plus` (
  `survey_questions_options_plus_id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_questions_options_id` int(11) NOT NULL,
  `survey_questions_options_plus_text` varchar(400) NOT NULL,
  PRIMARY KEY (`survey_questions_options_plus_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf16 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `survey_questions_options_plus`
--

INSERT INTO `survey_questions_options_plus` (`survey_questions_options_plus_id`, `survey_questions_options_id`, `survey_questions_options_plus_text`) VALUES
(1, 21, 'I''m living with my parents'),
(2, 21, 'I''m living in a flat share community'),
(3, 21, 'I''m living with a partner');

-- --------------------------------------------------------

--
-- Estrutura da tabela `survey_user`
--

CREATE TABLE IF NOT EXISTS `survey_user` (
  `survey_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`survey_user_id`),
  KEY `user_id` (`user_id`),
  KEY `survey_id` (`survey_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf16 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `survey_user`
--

INSERT INTO `survey_user` (`survey_user_id`, `survey_id`, `user_id`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `survey_user_answers`
--

CREATE TABLE IF NOT EXISTS `survey_user_answers` (
  `survey_user_answers_id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_user_id` int(11) NOT NULL,
  `survey_questions_id` int(11) NOT NULL,
  `survey_questions_options_id` int(11) NOT NULL,
  `survey_questions_options_plus` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`survey_user_answers_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf16 AUTO_INCREMENT=9 ;

--
-- Extraindo dados da tabela `survey_user_answers`
--

INSERT INTO `survey_user_answers` (`survey_user_answers_id`, `survey_user_id`, `survey_questions_id`, `survey_questions_options_id`, `survey_questions_options_plus`) VALUES
(1, 1, 1, 6, 'teacher'),
(2, 1, 2, 8, ''),
(3, 1, 3, 12, ''),
(4, 1, 4, 15, ''),
(5, 1, 4, 16, ''),
(6, 1, 4, 19, ''),
(7, 1, 5, 21, '3'),
(8, 1, 6, 24, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(30) NOT NULL DEFAULT '',
  `lastname` varchar(30) NOT NULL DEFAULT '',
  `email` varchar(80) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf16 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`user_id`, `firstname`, `lastname`, `email`, `password`) VALUES
(1, 'marcelo', 'cruz', 'mca.crz@gmail.com', '123456'),
(2, 'outro', 'usuario', 'test@test.com', '654321');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
