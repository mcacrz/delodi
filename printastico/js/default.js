function validate(){
	vc_error = '';
	if(document.frm.txt_email.value.length == 0){
		vc_error += 'Inform your email.\n';
	}
	
	if(document.frm.txt_password.value.length == 0 || document.frm.txt_password.length < 6){
		vc_error += 'Inform your password.\n';	
	}
	
	if(vc_error.length > 0){
		alert('PLEASE!! \n' + vc_error);
	}else{
		document.frm.btn_send.disabled = true;
		document.frm.submit();	
	}
}