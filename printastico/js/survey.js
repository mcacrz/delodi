function viewExtraOption(id_option){
	for(x = 0; x < document.getElementsByClassName('extra').length; x++){
		if(document.getElementsByClassName('extra')[x].type != 'text'){
			document.getElementsByClassName('extra')[x].style.display = 'none';
		}
	}
	
	for(x = 0; x < document.getElementsByClassName(id_option).length; x++){
		document.getElementsByClassName(id_option)[x].style.display = 'block';	
	}
}

function viewHideDiv(id_div){
	for(x = 0; x < document.getElementsByTagName('div').length; x++){
		document.getElementsByTagName('div')[x].style.display = 'none';
	}
	document.getElementById('div_'+id_div).style.display = 'block';	
}

function verify(){
	it_answered = false;
	vc_error = '';
	for(x = 0;x < document.getElementsByTagName('input').length;x++){
		className = document.getElementsByTagName('input')[x].className;
		if((document.getElementsByTagName('input')[x].type != 'button' && document.getElementsByTagName('input')[x].type != 'hidden') && className.search('extra') == -1){
			for(y = 0;y<document.getElementsByClassName(className).length;y++){
				if(document.getElementsByClassName(className)[y].checked == true){
					if($('.' + className, document).eq(y).attr('plus')){
					  clsPlus = $('.' + className, document).eq(y).attr('plus');
					  for(z = 0; z < document.getElementsByClassName(clsPlus).length; z++){
							if((document.getElementsByClassName(clsPlus)[z].type == 'radio' && document.getElementsByClassName(clsPlus)[z].checked == true)||(document.getElementsByClassName(clsPlus)[z].type == 'text' && document.getElementsByClassName(clsPlus)[z].value.length > 0)){
								it_answered = true;
								z = document.getElementsByClassName(clsPlus).length;
							}
					  }
					  if(it_answered == false){
						  vc_error += '- Please answer the extra option of the question ' + className + '.\n';
						  it_answered = true;
					  }
					}else{
						it_answered = true;
						y = document.getElementsByClassName(className).length;
					}
				}
			}
			if(it_answered == false){
				vc_error += '- Please answer the question ' + className + '.\n';
			}else{
				it_answered = false;
			}
			
			x += document.getElementsByClassName(className).length - 1;
		}
	}
	
	if(vc_error.length > 0){
		alert(vc_error);	
	}else{
		document.getElementById('bt_send').disabled == true;
		document.frm.submit();
	}
}