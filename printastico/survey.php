﻿<?php
require("conn_open.php");
require('active_user.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Delodi´s Survey</title>
<script language="javascript" src="js/jquery-1.11.1.min.js"></script>
<script language="javascript" src="js/survey.js"></script>
</head>
<body>
<?php
require('login_menu.php');
?>
<form name="frm" method="post" action="code/survey.php" target="_self">
	<input type="hidden" name="survey_id" value="1" />
<?php
$sql_survey = mysqli_query($con,"SELECT survey_questions_id, survey_questions_text, survey_questions_type FROM survey_questions WHERE survey_id = 1");
if(mysqli_num_rows($sql_survey) > 0){
	$x = 0;
	$y = 0;
	$z = 0;	
	$actual = 1;
	$before = 1;
	echo "<div id='div_".$actual."' style='margin:0 25% 0; display:block;'><table width='100%' border='1'>";
	while($sql_survey_line = mysqli_fetch_array($sql_survey)){
		$z += 1;  
		if($actual != $before){
			echo "</table></div>";
			echo "<div id='div_".$actual."' style='margin:0 25% 0; display:none;'><table width='100%' border='1'>";
			$before = $actual;
		}
		
		echo "<tr>"; 
		echo "<td style='width:50%;'>".$z. ' - ' . $sql_survey_line["survey_questions_text"] . "</td>";
		echo "<td style='width:50%;'><input type='hidden' name='survey_question_".$sql_survey_line["survey_questions_id"]."' value='".$sql_survey_line["survey_questions_id"]."' />";
		
		$sql_survey_options = mysqli_query($con,"SELECT survey_questions_options_id,survey_questions_options_text FROM survey_questions_options WHERE survey_questions_id = " . $sql_survey_line["survey_questions_id"]. " ORDER BY survey_questions_options_id");
		if(mysqli_num_rows($sql_survey_options) > 0){
			while($sql_survey_options_line = mysqli_fetch_array($sql_survey_options)){
				switch ($sql_survey_line["survey_questions_type"]) {
					case 1:
						if($sql_survey_options_line["survey_questions_options_text"] == 'other'){
							echo "<input type='radio' name='opt_radio_".$sql_survey_line["survey_questions_id"]."' value='".$sql_survey_options_line["survey_questions_options_id"]."' class='".$sql_survey_line["survey_questions_id"]."' plus='extra ".$sql_survey_options_line["survey_questions_options_id"]."'>".$sql_survey_options_line["survey_questions_options_text"]." <input type='text' name='opt_plus_radio_".$sql_survey_options_line["survey_questions_options_id"]."' value='' class='extra ".$sql_survey_options_line["survey_questions_options_id"]."'>";
						}else{
							echo "<input type='radio' name='opt_radio_".$sql_survey_line["survey_questions_id"]."' value='".$sql_survey_options_line["survey_questions_options_id"]."' class='".$sql_survey_line["survey_questions_id"]."'>".$sql_survey_options_line["survey_questions_options_text"];
						}
						echo "<br>";
						break;
					case 2:
						if($sql_survey_options_line["survey_questions_options_text"] == 'other'){
							echo "<input type='checkbox' name='opt_checkbox_".$sql_survey_options_line["survey_questions_options_id"]."' value='".$sql_survey_options_line["survey_questions_options_id"]."' class='".$sql_survey_line["survey_questions_id"]."' plus='extra ".$sql_survey_options_line["survey_questions_options_id"]."'>".$sql_survey_options_line["survey_questions_options_text"]." <input type='text' name='opt_plus_checkbox_".$sql_survey_options_line["survey_questions_options_id"]."' value='' class='extra ".$sql_survey_options_line["survey_questions_options_id"]."'>";
						}else{
							echo "<input type='checkbox' name='opt_checkbox_".$sql_survey_options_line["survey_questions_options_id"]."' value='".$sql_survey_options_line["survey_questions_options_id"]."' class='".$sql_survey_line["survey_questions_id"]."'>".$sql_survey_options_line["survey_questions_options_text"];
						}
						echo "<br>";
						break;
					case 3:
						$sql_survey_options_plus = mysqli_query($con,"SELECT survey_questions_options_plus_id, survey_questions_options_plus_text FROM survey_questions_options_plus WHERE survey_questions_options_id = ".$sql_survey_options_line["survey_questions_options_id"]);
						  if(mysqli_num_rows($sql_survey_options_plus) > 0){
	  							echo "<input type='radio' name='opt_radio_".$sql_survey_line["survey_questions_id"]."' value='".$sql_survey_options_line["survey_questions_options_id"]."' onclick='viewExtraOption(".$sql_survey_options_line["survey_questions_options_id"].");' class='".$sql_survey_line["survey_questions_id"]."' plus='extra ". $sql_survey_options_line["survey_questions_options_id"] ."'>".$sql_survey_options_line["survey_questions_options_text"]."<br>";
								while($sql_survey_options_plus_line = mysqli_fetch_array($sql_survey_options_plus)){
									echo "<input type='radio' name='opt_plus_radio_".$sql_survey_options_line["survey_questions_options_id"]."' value='".$sql_survey_options_plus_line["survey_questions_options_plus_id"]."' class='extra ". $sql_survey_options_line["survey_questions_options_id"] ."' style='margin:0 0 0 20px; display:none;'><span class='extra ". $sql_survey_options_line["survey_questions_options_id"] ."' style='display:none;'>".$sql_survey_options_plus_line["survey_questions_options_plus_text"]."</span><br>";
								}
						  }else{
	  							echo "<input type='radio' name='opt_radio_".$sql_survey_line["survey_questions_id"]."' value='".$sql_survey_options_line["survey_questions_options_id"]."' onclick='viewExtraOption(".$sql_survey_options_line["survey_questions_options_id"].");' class='".$sql_survey_line["survey_questions_id"]."'>".$sql_survey_options_line["survey_questions_options_text"]."<br>";							  
						  }
						break;							
				}
					
			}
		}
		
		echo "</td>";
		echo "</tr>";
		
		$x += 1;
		$y += 1;
		
		if($y == 3){
			if($x == mysqli_num_rows($sql_survey)){
					if(($actual - 1) > 0) { 
						echo "<tr><td align='center' colspan='2'><input type='button' name='bt_page_". ($actual - 1) ."' value='PAGE ". ($actual - 1) ."' onclick='viewHideDiv(". ($actual - 1) .");'>";
					}

					echo "<input type='button' name='bt_send' id='bt_send' value='SEND' onclick='verify();'></td></tr>";
			}else{
					if(($actual - 1) > 0) { 
						echo "<tr><td align='center' colspan='2'><input type='button' name='bt_page_". ($actual - 1) ."' value='PAGE ". ($actual - 1) ."' onclick='viewHideDiv(". ($actual - 1) .");'></td></tr>";
					}
					
					echo "<tr><td align='center' colspan='2'><input type='button' name='bt_page_". ($actual + 1) ."' value='PAGE ". ($actual + 1) ."' onclick='viewHideDiv(". ($actual + 1) .");'></td></tr>";

			}
			$y = 0;
			$actual += 1;
		}
		
	}
}
?>
</form>
</body>
</html>