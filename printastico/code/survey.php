﻿<?php
require('../conn_open.php');
require('../active_user.php');

$survey_id = $_POST['survey_id'];
$questions = '';
$question_id = 0;
$options = '';
$plus = '';

mysqli_query($con,"INSERT INTO survey_user (survey_id,user_id) VALUES (".$survey_id.",".$user_id.")");

$sql_survey_user = mysqli_query($con,"SELECT survey_user_id FROM survey_user ORDER BY survey_user_id limit 1");
$sql_survey_user_line = mysqli_fetch_assoc($sql_survey_user);
$survey_user_id = $sql_survey_user_line['survey_user_id'];
mysqli_free_result($sql_survey_user);

foreach(array_keys($_POST) as $field){
	if (strpos($field,'survey_question') === 0){
		$question_id = $_POST[$field];
	}else if(strpos($field,'opt_radio') === 0){
		if($options == ''){
			$options = $question_id.'|'.$_POST[$field];	
		}else{
			$options .= ','.$question_id.'|'. $_POST[$field];	
		}
		
		if($_POST['opt_plus_radio_'.$_POST[$field]] != ''){
			$options .= '|'.$_POST['opt_plus_radio_'.$_POST[$field]];
		}else{
			$options .= '|0';
		}
	}else if(strpos($field,'opt_checkbox') === 0){
		if($options == ''){
			$options = $question_id.'|'.$_POST[$field];
		}else{
			$options .= ','.$question_id.'|'. $_POST[$field];
		}

		if($_POST['opt_plus_checkbox_'.$_POST[$field]] != ''){
			$options .= '|'.$_POST['opt_plus_checkbox_'.$_POST[$field]];
		}else{
			$options .= '|0';
		}
	}
}

$values = explode(',',$options);

foreach($values as $line){
	$column = explode('|',$line);
	$qry = "INSERT INTO survey_user_answers (survey_user_id,survey_questions_id,survey_questions_options_id,survey_questions_options_plus) VALUES (".$survey_user_id.",".$column[0].",".$column[1].",";
	if($column[2] == '0'){
		$qry .= "'')";	
	}else{
		$qry .= "'".$column[2]."')";
	}

	mysqli_query($con,$qry);
}

header("location:../home.php?it_survey=1");

require('../conn_close.php');
?>